import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import './widgets/Weather.dart';

void main() {
  runApp(new MaterialApp(
      title: 'MyCast',
      theme: ThemeData(
        primarySwatch: Colors.grey,
      ),
      home: new Scaffold(
          backgroundColor: Color(0xFF003559),
          appBar: new AppBar(
            title: new Text('MyCast'),
          ),
          body: new Weather())));
}
