import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:location/location.dart';
import 'package:flutter/services.dart';

import 'weatherInfo.dart';

import '../models/WeatherData.dart';
import '../models/ForecastData.dart';

class Weather extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new WeatherState();
  }
}

class WeatherState extends State<Weather> {
  bool isLoading = false;
  WeatherData weatherData;
  ForecastData forecastData;
  Location _location = new Location();
  String error;
  final String appId = 'ae027a576f9f5f6663b0e69761ac53d5';
  final String urlHeader = 'https://api.openweathermap.org/data/2.5/';

  @override
  void initState() {
    super.initState();

    loadWeather();
  }

  loadWeather() async {
    setState(() {
      isLoading = true;
    });

    var location;

    try {
      location = await _location.getLocation();
      error = null;
    } on PlatformException catch (e) {
      if (e.code == 'PERMISSION_DENIED') {
        error = 'Permission denied';
      } else if (e.code == 'PERMISSION_DENIED_NEVER_ASK') {
        error =
            'Permission denied - please ask the user to enable it from app settings';
      }
      location = null;
    }

    if (location != null) {
      final lat = location.latitude;
      final lon = location.longitude;
      print('Latitude: $lat');
      print('Longitude: $lon');

      final weatherRes = await http.get(urlHeader +
          'weather?APPID=' +
          appId +
          '&lat=${lat.toString()}&lon=${lon.toString()}&units=imperial');

      final forecastRes = await http.get(urlHeader +
          'forecast?APPID=' +
          appId +
          '&lat=${lat.toString()}&lon=${lon.toString()}&units=imperial');

      if (weatherRes.statusCode == 200 && forecastRes.statusCode == 200) {
        return setState(() {
          weatherData = new WeatherData.fromJson(jsonDecode(weatherRes.body));
          forecastData =
              new ForecastData.fromJson(jsonDecode(forecastRes.body));
          isLoading = false;
        });
      }
    }
    setState(() {
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: weatherData != null
                      ? Forecast(weather: weatherData, textColor: Colors.white)
                      : Container(),
                ),
                Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: isLoading
                        ? CircularProgressIndicator(
                            strokeWidth: 2.0,
                            valueColor:
                                new AlwaysStoppedAnimation(Colors.white),
                          )
                        : IconButton(
                            icon: new Icon(Icons.refresh),
                            tooltip: 'Refresh',
                            onPressed: () => loadWeather,
                            color: Colors.white))
              ],
            ),
          ),
          SafeArea(
              child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                      height: 200.0,
                      child: forecastData != null
                          ? ListView.builder(
                              itemCount: forecastData.list.length,
                              scrollDirection: Axis.horizontal,
                              itemBuilder: (context, index) => TenDayForecast(
                                  weather: forecastData.list.elementAt(index)))
                          : Forecast(
                              weather: weatherData, textColor: Colors.white)))),
        ],
      ),
    );
  } // build()
} // MyAppState
