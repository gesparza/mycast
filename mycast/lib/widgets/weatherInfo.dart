import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../models/WeatherData.dart';

class Forecast extends StatelessWidget {
  const Forecast({Key key, @required this.weather, @required this.textColor})
      : super(key: key);

  final WeatherData weather;
  final Color textColor;

  @override
  Widget build(BuildContext context) {
    return new Column(
      children: <Widget>[
        Text(weather.name, style: new TextStyle(color: this.textColor)),
        Text(weather.main,
            style: new TextStyle(color: this.textColor, fontSize: 32.0)),
        Text('${weather.temp.toString()}°F',
            style: new TextStyle(color: this.textColor)),
        Image.network('https://openweathermap.org/img/w/${weather.icon}.png'),
        Text(new DateFormat.yMMMd().format(weather.date),
            style: new TextStyle(color: this.textColor)),
        Text(new DateFormat.Hm().format(weather.date),
            style: new TextStyle(color: this.textColor)),
      ],
    );
  } // build()
} // InputPicker

class TenDayForecast extends StatelessWidget {
  TenDayForecast({Key key, @required this.weather}) : super(key: key);

  final WeatherData weather;

  @override
  Widget build(BuildContext context) {
    return Card(
        color: Color(0xFF006DAA),
        child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Forecast(
                weather: weather,
                textColor: Colors.white)));
  } // build()
} // TenDayForecast
